﻿function isJavaInstalled()
{
    $result = $false;
    # Current java versions
    $targeted_java_versions = @("1.8", "1.9", "1.10", "1.11");

    # Test if javais installed
    $java_version = & java -version 2>&1;

    #Write-Host( '***' + $java_version + '***');

    # If java is installed "java -version" will return 6 line response
    if( $java_version.Count -gt 1 )
    {
        # Iterate over list of JDK versions
        foreach( $version in $targeted_java_versions )
        {
            # Look for one of targeted jdk versions 
            $jvalue = select-string -pattern $version -InputObject $java_version.Item(1);

            # If one of the targeted versions was found in the java -version response, then return true
            if( $jvalue.Line.Length -gt 0 )
            {
                Write-Host( "JDK 8+ installed[" + $jvalue + "]");
                $result = $true;
            }
        }
    }

    return $result;
}

$test = isJavaInstalled;

if( $test )
{
    Write-Host("Java is installed");
}
else
{
    Write-Host("Java is not installed");
}